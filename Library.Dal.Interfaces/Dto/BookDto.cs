﻿namespace Library.Dal.Interfaces.Dto
{
    public class BookDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public int Pages { get; set; }
        public int Quantity { get; set; }
        public string Genre { get; set; }
        public int? LoanedQuantity { get; set; }
        public bool? Owns { get; set; }
    }
}
