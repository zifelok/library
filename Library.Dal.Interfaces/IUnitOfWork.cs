﻿namespace Library.Dal.Interfaces
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
