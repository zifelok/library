﻿using Library.Dal.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Dal.Interfaces
{
    public interface IBookLoanRepository
    {
        BookLoan Get(int userId, int bookId);
        void Add(BookLoan bookLoan);
        void Delete(int userId, int bookId);
        int GetUserLoanCount(int userId);
    }
}
