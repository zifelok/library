﻿using AutoMapper;
using Library.BL.Interfaces.Dto;
using Library.BL.Interfaces.Services;
using Library.Dal.Entities;
using Library.Dal.Interfaces;
using System.Collections.Generic;

namespace Library.BL.Services
{
    public class BookService : IBookSevice
    {
        public readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly IBookRepository bookRepository;
        private readonly IBookLoanRepository bookLoanRepository;

        public BookService(IUnitOfWork unitOfWork, IMapper mapper, IBookRepository bookRepository, IBookLoanRepository bookLoanRepository)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.bookRepository = bookRepository;
            this.bookLoanRepository = bookLoanRepository;
        }

        public BookDto Add(BookInputDto book)
        {
            var entity = mapper.Map<Book>(book);
            bookRepository.Add(entity);
            unitOfWork.Commit();
            return mapper.Map<BookDto>(entity);
        }

        public void Delete(int id)
        {
            bookRepository.Delete(id);
            unitOfWork.Commit();
        }

        public IReadOnlyCollection<BookDto> Get(string orderby = null, bool asc = true, int? userId = null)
        {
            var entities = bookRepository.GetBookDtos(orderby, asc, userId);
            return mapper.Map<IReadOnlyCollection<BookDto>>(entities);
        }

        public BookDto Get(int id)
        {
            var entity = bookRepository.Get(id);
            return mapper.Map<BookDto>(entity);
        }

        public void Loan(int id, int userId)
        {
            var loan = bookLoanRepository.Get(userId, id);
            if (loan == null)
            {
                var count = bookLoanRepository.GetUserLoanCount(userId);
                if (count < 3)
                {
                    bookLoanRepository.Add(new Dal.Entities.BookLoan()
                    {
                        BookId = id,
                        UserId = userId
                    });
                    unitOfWork.Commit();
                }
            }
        }

        public void Return(int id, int userId)
        {
            var loan = bookLoanRepository.Get(userId, id);
            if (loan != null)
            {
                bookLoanRepository.Delete(userId, id);
                unitOfWork.Commit();
            }
        }

        public BookDto Update(BookInputDto book)
        {
            var entity = mapper.Map<Book>(book);
            bookRepository.Update(entity);
            unitOfWork.Commit();
            return mapper.Map<BookDto>(entity);
        }
    }
}
