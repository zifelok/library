﻿using System.Collections.Generic;

namespace Library.Dal.Entities
{
    public class Role
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
