﻿using System.Collections.Generic;

namespace Library.Dal.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordHash { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<BookLoan> BookLoans { get; set; }
    }
}
