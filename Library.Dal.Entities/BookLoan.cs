﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Dal.Entities
{
    public class BookLoan
    {
        public int BookId { get; set; }
        public Book Book { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public DateTime Date { get; set; }
        public DateTime EndDate { get; set; }
    }
}
