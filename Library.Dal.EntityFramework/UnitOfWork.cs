﻿using Library.Dal.Interfaces;

namespace Library.Dal.EntityFramework
{
    public class UnitOfWork : IUnitOfWork
    {
        public LibraryDBContext context;

        public UnitOfWork(LibraryDBContext context)
        {
            this.context = context;
        }
        public void Commit()
        {
            context.SaveChanges();
        }
    }
}
