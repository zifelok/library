﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Library.Dal.Entities;
using Library.Dal.Interfaces;
using Library.Dal.Interfaces.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Dal.EntityFramework.Repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly LibraryDBContext dBContext;
        private readonly IMapper mapper;

        public BookRepository(IMapper mapper, LibraryDBContext dBContext)
        {
            this.dBContext = dBContext;
            this.mapper = mapper;
        }

        public Book Add(Book book)
        {
            dBContext.Add(book);
            return book;
        }

        public void Delete(int id)
        {
            var book = dBContext.Books.Include(b => b.BookLoans).FirstOrDefault(b => b.Id == id);

            var loans = book.BookLoans;

            foreach (var loan in loans)
            {
                dBContext.Remove(loan);
            }

            dBContext.Remove(book);
        }

        public Book Get(int id)
        {
            return dBContext.Books.AsNoTracking().FirstOrDefault(b => b.Id == id);
        }

        public IReadOnlyCollection<BookDto> GetBookDtos(string orderby = null, bool asc = true, int? userId = null)
        {
            var query = dBContext.Books.AsNoTracking();
            if (orderby == nameof(Book.Genre))
            {
                query = asc ? query.OrderBy(b => b.Genre) : query.OrderByDescending(b => b.Genre);
            }
            else if (orderby == nameof(Book.Author))
            {
                query = asc ? query.OrderBy(b => b.Author) : query.OrderByDescending(b => b.Author);
            }
            return SelectDto(query, userId).ToList();
        }

        public void Update(Book entity)
        {
            dBContext.Attach(entity);
            dBContext.Entry(entity).State = EntityState.Modified;
        }

        private IQueryable<BookDto> SelectDto(IQueryable<Book> books, int? userId = null)
        {
            return books.Select(b => new BookDto()
            {
                Author = b.Author,
                Genre = b.Genre,
                Id = b.Id,
                LoanedQuantity = b.BookLoans.Count(),
                Owns = b.BookLoans.Any(bl => bl.UserId == userId),
                Pages = b.Pages,
                Quantity = b.Quantity,
                Title = b.Title
            });
        }
    }
}
