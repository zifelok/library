﻿using System;
using Library.Dal.Entities;
using Microsoft.EntityFrameworkCore;

namespace Library.Dal.EntityFramework
{
    internal static class Seed
    {
        public static void SeedData(ModelBuilder modelBuilder)
        {
            SeedRoles(modelBuilder);
            SeedUsers(modelBuilder);
            SeedUserRoles(modelBuilder);
            SeedBooks(modelBuilder);
        }

        private static void SeedBooks(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>().HasData(
                new Book { Id = 1, Author = "Author1", Genre = "Genre1", Pages = 128, Quantity = 10, Title = "Title01" },
                new Book { Id = 2, Author = "Author1", Genre = "Genre1", Pages = 128, Quantity = 10, Title = "Title02" },
                new Book { Id = 3, Author = "Author1", Genre = "Genre1", Pages = 989, Quantity = 10, Title = "Title03" },
                new Book { Id = 4, Author = "Author2", Genre = "Genre2", Pages = 128, Quantity = 10, Title = "Title04" },
                new Book { Id = 5, Author = "Author2", Genre = "Genre1", Pages = 546, Quantity = 10, Title = "Title05" },
                new Book { Id = 6, Author = "Author1", Genre = "Genre3", Pages = 128, Quantity = 1000, Title = "Title06" },
                new Book { Id = 7, Author = "Author3", Genre = "Genre1", Pages = 128, Quantity = 10, Title = "Title07" },
                new Book { Id = 8, Author = "Author3", Genre = "Genre2", Pages = 128, Quantity = 100, Title = "Title08" },
                new Book { Id = 9, Author = "Author3", Genre = "Genre1", Pages = 128, Quantity = 10, Title = "Title09" },
                new Book { Id = 10, Author = "Author4", Genre = "Genre1", Pages = 128, Quantity = 10, Title = "Title10" }
                );
        }

        private static void SeedRoles(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(
                new Role { Id = 1, Title = "User" },
                new Role { Id = 2, Title = "Administrator" }
                );
        }

        private static void SeedUsers(ModelBuilder modelBuilder)
        {
            //Default password - "password"
            modelBuilder.Entity<User>().HasData(
                new User { Id = 1, UserName = "administrator", PasswordSalt = "c414ff1fea07d5e1017f6b9f0447f35e", PasswordHash = "e3bc0bb427ff253d9cfef8d9d968866cbe2be5182c64054762db5063a50c7337" },
                new User { Id = 2, UserName = "user", PasswordSalt = "759d075c9ee2718de32713c981471c3f", PasswordHash = "5034083205dd89569f9911390cf10ffbd9f96cd8a1bfddf3cab8348673a51f5d" }
                );
        }

        private static void SeedUserRoles(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>().HasData(
                new UserRole { UserId = 1, RoleId = 1 },
                new UserRole { UserId = 1, RoleId = 2 },
                new UserRole { UserId = 2, RoleId = 1 }
                );
        }
    }
}
