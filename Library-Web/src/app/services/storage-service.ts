import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private static tokenKey = 'token';
  private static storage = window.sessionStorage;

  public getToken(): string {
    return StorageService.storage.getItem(StorageService.tokenKey);
  }

  public setToken(token: string): void {
    if (token) {
      StorageService.storage.setItem(StorageService.tokenKey, token);
    } else {
      StorageService.storage.removeItem(StorageService.tokenKey);
    }
  }
}
