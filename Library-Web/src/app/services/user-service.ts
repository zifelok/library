import {Injectable} from '@angular/core';
import {ApiClientHelper} from './api-client-helper';
import {User} from '../models/user/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private static currentUser: User;

  constructor(private apiClientHelper: ApiClientHelper) {
  }

  public getCurrentUser(refresh?: boolean): Promise<User> {
    if (!UserService.currentUser) {
      refresh = true;
    }
    if (refresh) {
      return this.apiClientHelper.get<User>('/users/current').toPromise()
        .then(d => UserService.currentUser = d)
        .catch(e => UserService.currentUser = null);
    } else {
      return new Promise(() => UserService.currentUser);
    }
  }

  public isAuthenticated(): boolean {
    return !!UserService.currentUser;
  }

  public isAdministrator(): boolean {
    return this.isAuthenticated() && this.isUserInRole('Administrator');
  }

  public isUser(): boolean {
    return this.isAuthenticated() && this.isUserInRole('User');
  }

  public isUserInRole(role: string): boolean {
    return this.isAuthenticated() && UserService.currentUser.roles.filter(r => r === role).length > 0;
  }
}
