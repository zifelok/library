import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiClientHelper} from './api-client-helper';
import {Observable} from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  constructor(private apiClientHelper: ApiClientHelper) {
  }

  public login(loginModel: { userName: string, password: string }): Observable<{ token: string }> {
    return this.apiClientHelper.post<{ token: string }>('/token', loginModel);
  }
}
