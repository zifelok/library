import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StorageService} from './storage-service';
import {environment} from '../../environments/environment';
import {Observable} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class ApiClientHelper {
  constructor(private httpClient: HttpClient,
              private storageService: StorageService) {

  }

  private buildOptions() {
    return {headers: this.buildHeaders()};
  }

  public buildHeaders(): HttpHeaders {
    let headers = new HttpHeaders({
      'Cache-Control': 'no-cache',
      'Pragma': 'no-cache',
      'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT'
    });
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    const token = this.storageService.getToken();
    if (token) {
      headers = headers.set('Authorization', 'Bearer ' + token);
    }
    return headers;
  }

  public buildAbsoluteUrl(url: string): string {
    return environment.apiUrl + url;
  }

  public post<T>(url: string, body: any | null): Observable<T> {
    url = this.buildAbsoluteUrl(url);
    return this.httpClient.post<T>(url, body, this.buildOptions());
  }

  public put<T>(url: string, body: any | null): Observable<T> {
    url = this.buildAbsoluteUrl(url);
    return this.httpClient.put<T>(url, body, this.buildOptions());
  }

  public get<T>(url: string): Observable<T> {
    url = this.buildAbsoluteUrl(url);
    return this.httpClient.get<T>(url, this.buildOptions());
  }

  public delete<T>(url: string): Observable<T> {
    url = this.buildAbsoluteUrl(url);
    return this.httpClient.delete<T>(url, this.buildOptions());
  }
}
