import {Component, Input, Output} from '@angular/core';
import {TokenService} from '../../services/token-service';
import {StorageService} from '../../services/storage-service';
import {UserService} from '../../services/user-service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent {
  @Input()
  public loginModel = {userName: '', password: ''};
  @Output()
  public errorMessage = '';

  constructor(private tokenService: TokenService,
              private storageService: StorageService,
              private userService: UserService,
              private router: Router) {
  }


  public login(): void {
    this.tokenService.login(this.loginModel)
      .subscribe(d => this.loginSuccess(d.token), this.loginFailure);
  }

  private loginSuccess = (token: string) => {
    this.storageService.setToken(token);
    this.userService.getCurrentUser(true)
      .then(() => this.router.navigate(['/']));
  }

  private loginFailure = () => {
    this.errorMessage = 'Please check your credentials.';
  }
}
