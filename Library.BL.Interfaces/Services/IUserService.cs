﻿using Library.BL.Interfaces.Dto.User;
using Library.Dal.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.BL.Interfaces.Services
{
    public interface IUserService
    {
        LoginResultDto Login(LoginDto loginDto);
        UserDto GetUser(int id);
    }
}
