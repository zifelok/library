﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.BL.Interfaces.Dto.User
{
    public class LoginResultDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public IReadOnlyCollection<string> Roles { get; set; }
    }
}
