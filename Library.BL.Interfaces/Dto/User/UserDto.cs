﻿using System.Collections.Generic;

namespace Library.BL.Interfaces.Dto.User
{
    public class UserDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public IReadOnlyCollection<string> Roles { get; set; }
    }
}
