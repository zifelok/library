﻿using Library.BL.Interfaces.Dto.User;
using Library.BL.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Library.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        [Authorize]
        [HttpGet("current")]
        public UserDto GetCurrent()
        {
            var userIdClaim = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            int userId;
            int.TryParse(userIdClaim, out userId);
            return userService.GetUser(userId);
        }
    }
}
