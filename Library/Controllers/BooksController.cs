﻿using Library.BL.Interfaces.Dto;
using Library.BL.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;

namespace Library.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBookSevice bookService;

        public BooksController(IBookSevice bookService)
        {
            this.bookService = bookService;
        }

        // GET: api/Books
        [HttpGet]
        public IReadOnlyCollection<BookDto> Get(string orderby = null, bool asc = true)
        {

            return bookService.Get(orderby, asc, GetUserId());
        }

        [HttpGet("{id}", Name = "Get")]
        public BookDto Get(int id)
        {
            return bookService.Get(id);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public BookDto Post([FromBody] BookInputDto book)
        {
            return bookService.Add(book);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPut("{id}")]
        public BookDto Put(int id, [FromBody] BookInputDto book)
        {
            book.Id = id;
            return bookService.Update(book);
        }

        // POST: api/Books/{id}/loan
        [Authorize]
        [HttpPost("{id}/loan")]
        public void Loan(int id)
        {
            bookService.Loan(id, GetUserId());
        }

        // POST: api/Books/{id}/return
        [Authorize]
        [HttpPost("{id}/return")]
        public void Return(int id)
        {
            bookService.Return(id, GetUserId());
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            bookService.Delete(id);
        }

        private int GetUserId()
        {
            var userIdClaim = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            int userId;
            int.TryParse(userIdClaim, out userId);
            return userId;
        }
    }
}
